<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	if(\Auth::user())
//     	return view('landing');
//     else
//     	return view('login');
// });

// Auth::routes();

Route::post('webportal/login', 'Auth\LoginController@postWebLogin');
Route::post('portal/login', 'Auth\LoginController@postLogin');
Route::post('portal/isUserLoggedIn', 'Auth\LoginController@isUserLoggedIn');
Route::post('portal/logout', 'Auth\LoginController@logout');
Route::get('home', 'Auth\LoginController@setLocalStorage');
Route::get('portal/mytest', 'Auth\LoginController@myTestCode');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


// Route::post('/login', function(){
// 	if(!\Auth::user())
// 		return view('login');
// 	else
// 		return view('landing');
// })->name('login');



//Route::view('landing', '/');

// Auth::routes();

Route::get('categories/getlist', 'CategoriesController@getList');
Route::post('challenges/create', 'ChallengeController@saveRecord');


Route::get('challenges/getlist', 'ChallengeController@getList');
Route::get('challenges/getDetails', 'ChallengeController@show');
Route::post('challenges/toggle-like', 'ChallengeController@toggleLike');
Route::post('challenges/comment/{comment_id}/owner-like', 'ChallengeController@toggleCommentOwnerLike');
Route::post('challenges/comment/{comment_id}/owner-tick', 'ChallengeController@toggleCommentOwnertick');
Route::post('challenges/comment/{comment_id}/owner-win', 'ChallengeController@toggleCommentOwnerwin');

Route::get('/posts','ChallengeController@gechallenge');
//    $user = \Auth::user();
//    $post_json = DB::table('challenges')->orderBy('challenges.created_at','desc')->take(4)->get();
//return $post_json;



Route::post('challenges/store-comment', 'ChallengeController@postComment');
Route::get('challenges/comments', 'ChallengeController@getComments');
Route::get('friends/getSuggestions/{total?}', 'ChallengeController@getFriendSuggestions');
Route::get('challenges/getChallenges/{total?}', 'ChallengeController@getChallenges');
Route::get('friends/getFriendsList/{total?}', 'ChallengeController@getFriends');
Route::post('friends/toggle-follow', 'ChallengeController@toggleFollow');
Route::get('user/followers-list', 'UsersController@getFollowers');


Route::post('user/update-profile', 'UsersController@updateProfile');
Route::get('user/get-profile/{id}', 'UsersController@getProfile');

Route::get('user/top-contributors', 'UsersController@topContributors');

Route::post('user/top-notifications', 'UsersController@topNotifications');

Route::get('/activities/get-list/{id?}', 'UsersController@getActivities');


Route::post('api/changePassword', 'Auth\passwordresetcontroller@changePassword');

Route::get('search/{searchParameter}','UsersController@searchUsers');

Route::get('api/users', 'UsersController@getusers');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('ui/admin')->group(function () {
	// die('herer');
 
     Route::get('/', 'Auth\LoginadminController@showLoginForm')->name('login');;
// Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	// Route::post('/login', 'Auth\LoginController@postLogin')->name('admin.login');;

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/categories', 'CategoriesController@index')->name('category_list');
Route::get('/admin/categories/add', 'CategoriesController@add')->name('categories_add');
Route::post('admin/categories/add', 'CategoriesController@store')->name('category_store');
Route::get('admin/categories/edit/{slug}', 'CategoriesController@edit');
Route::patch('admin/categories/edit/{slug}', 'CategoriesController@update')->name('category_update');
Route::post('admin/categories/delete', 'CategoriesController@destroy');
// USER ROUTES

Route::get('admin/users','UsersController@index');
Route::get('admin/users/add','UsersController@add');
Route::get('admin/users/edit/{slug}','UsersController@edit');
Route::post('admin/users/add','UsersController@store');
Route::patch('admin/users/edit/{slug}','UsersController@update');
Route::post('admin/users/delete', 'UsersController@destroy');

//CHANLENGES ROUTES 

Route::get('/admin/challenges', 'ChallengeController@index')->name('challenge_list');
Route::get('/admin/challenges/add', 'ChallengeController@add')->name('challenges_add');
Route::post('admin/challenges/add', 'ChallengeController@store');
Route::get('admin/challenges/edit/{slug}', 'ChallengeController@edit');
Route::patch('admin/challenges/edit/{slug}', 'ChallengeController@update')->name('challenge_update');
Route::post('admin/challenges/delete', 'ChallengeController@destroy');

Route::get('/admin/campaigns', 'CampaignController@index')->name('campaign_list');
Route::get('/admin/campaigns/add', 'CampaignController@add')->name('campaigns_add');
Route::post('admin/campaigns/add', 'CampaignController@store')->name('category_store');;
Route::get('admin/campaigns/edit/{slug}', 'CampaignController@edit');
Route::patch('admin/campaigns/edit/{slug}', 'CampaignController@update')->name('campaign_update');
Route::post('admin/campaigns/delete', 'CampaignController@destroy');
});

